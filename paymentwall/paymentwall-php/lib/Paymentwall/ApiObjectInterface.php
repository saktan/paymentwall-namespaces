<?php
namespace Paymentwall\ApiObjectInterface;

interface Paymentwall_ApiObjectInterface
{
	public function getCard();
}