<?php

namespace Paymentwall\ResponseSuccess;
use Paymentwall\ResponseAbstract\Paymentwall_Response_Abstract;
use Paymentwall\ResponseInterface\Paymentwall_Response_Interface;

class Paymentwall_Response_Success extends Paymentwall_Response_Abstract implements Paymentwall_Response_Interface
{
	public function process()
	{
		if (!isset($this->response)) {
			return $this->wrapInternalError();
		}

		$response = array(
			'success' => 1
		);

		return json_encode($response);
	}
}