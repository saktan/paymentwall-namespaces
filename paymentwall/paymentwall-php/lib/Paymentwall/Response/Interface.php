<?php

namespace Paymentwall\ResponseInterface;

interface Paymentwall_Response_Interface
{
	public function process();
}